const sliderToggle = (event) => {
    const target = event.target;
    const sliderContainer = document.querySelector(".slider__switches");
    const sliderSwitches = document.querySelectorAll(".slider__switch");
    const currentSlide = parseInt(target.getAttribute('data-slide'), 10);
    const totalSlide = sliderSwitches.length;
    //target.setAttribute('data-slide', '0');
    console.log(target.getAttribute("id"));
    if (target.getAttribute("id") === 'slideSwitch3') {
        sliderContainer.classList.remove("slider__switches--toggle");
        sliderSwitches.forEach(sliderSwitch => {
            let currentValue = parseInt(sliderSwitch.getAttribute('data-slide'), 10);
            let count = currentSlide > 0 ? 1 : 0;
            sliderSwitch.setAttribute('data-slide', currentValue+count);
        });
        target.setAttribute('data-slide', '0');
        sliderContainer.classList.add("slider__switches--toggle");
    }
};

export default sliderToggle;