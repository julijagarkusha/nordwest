import "./styles/index.scss";
import "./utils/sliderToggle";
import sliderToggle from "./utils/sliderToggle";

const bodyElement = document.querySelector("body");
const navElement = document.querySelector("#headMenu");
const menuItems = navElement.childNodes;
const mobileMenuElement = document.querySelector(".menu--mobile");
const navigationElement = document.querySelector(".navigation");
const sliderSwitches = document.querySelectorAll(".slider__switch");

const subMenuToggle = (event) => {
    const target = event.target;
    const subMenu = target.nextSibling;
    const targetParent = target.parentNode.parentNode;
    console.log(target.parentNode.classList.contains("menu--sub"));
    if (target.classList.contains("menu--sub") || targetParent.classList.contains("menu--sub")) {
        return;
    }
    if (target.classList.contains("menu__link--active")) {
        target.classList.remove("menu__link--active");
        subMenu ? subMenu.classList.remove("menu--show") : "";
    } else {
        menuItems.forEach(menuItem => {
            const links = menuItem.childNodes[0];
            const menu = menuItem.childNodes[1];
            menu ? menu.classList.remove("menu--show") : "";
            links.classList.remove("menu__link--active");
        });
        target.classList.add("menu__link--active");
        subMenu ? subMenu.classList.add("menu--show") : "";
    }
};

const mobileMenuToggle = () => {
    if(navigationElement.classList.contains("navigation--show") ) {
        navigationElement.classList.remove("navigation--show")
    } else {
        navigationElement.classList.add("navigation--show");
        bodyElement.classList.add("body--mask");
    }
};

const mobileMenuHidden = (event) => {
    const target = event.target;
    if (  target.classList.contains("body--mask")) {
        navigationElement.classList.remove("navigation--show");
        bodyElement.classList.remove("body--mask");
    }
};

menuItems.forEach(menuItem => {
   menuItem.addEventListener('click', subMenuToggle);
});

mobileMenuElement.addEventListener("click", mobileMenuToggle);
bodyElement.addEventListener("click", mobileMenuHidden);

sliderSwitches.forEach(sliderSwitch => {
   sliderSwitch.addEventListener("click", sliderToggle)
});
